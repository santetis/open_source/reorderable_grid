import 'package:flutter/material.dart';
import 'package:reorderable_grid/reorderable_grid.dart';
import 'package:reorderable_grid/src/bloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');

  List<LetterWidget> grid = [];
  ReorderableGridBloc reorderableGridBloc;
  @override
  void initState() {
    super.initState();
    grid = alphabet
        .map(
          (letter) => LetterWidget(
                // key: UniqueKey(),
                letter: letter,
              ),
        )
        .toList();

    reorderableGridBloc = ReorderableGridBloc(
      children: grid,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Draggable grid element'),
      ),
      body: ReorderableGrid(
        reorderableGridBloc: reorderableGridBloc,
      ),
    );
  }
}

class LetterWidget extends StatefulWidget {
  final String letter;

  LetterWidget({
    @required this.letter,
    Key key,
  }) : super(key: key);

  @override
  LetterWidgetState createState() => LetterWidgetState();
}

class LetterWidgetState extends State<LetterWidget> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final width = size.width / 4;
    final height = width;
    final reorderableGridBloc =
        ReorderableGridProvider.of(context).reorderableGridBloc;
    return GridItem(
      builder: (context, snapshot) {
        if (snapshot.data == widget) {
          return Container();
        }
        return Draggable<Widget>(
          key: widget.key,
          data: widget,
          childWhenDragging: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(),
          ),
          onDragStarted: () {
            reorderableGridBloc.draggedWidgetSink.add(widget);
          },
          feedback: Container(
            decoration: BoxDecoration(
              color: Colors.blue,
              border: Border.all(color: Colors.red),
              borderRadius: BorderRadius.all(Radius.circular(20)),
            ),
            width: width,
            height: height,
            child: Center(
              child: Text(
                widget.letter,
                style: TextStyle(
                  color: Colors.white,
                  decoration: TextDecoration.none,
                ),
              ),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.all(Radius.circular(20)),
              ),
              width: width,
              height: height,
              child: Center(
                child: Text(
                  widget.letter,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
