import 'package:flutter/widgets.dart';
import 'package:reorderable_grid/src/bloc.dart';

class ReorderableGridProvider extends InheritedWidget {
  static ReorderableGridProvider of(BuildContext context) =>
      context.inheritFromWidgetOfExactType(ReorderableGridProvider);

  final ReorderableGridBloc reorderableGridBloc;

  ReorderableGridProvider({this.reorderableGridBloc, Widget child})
      : super(child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}
