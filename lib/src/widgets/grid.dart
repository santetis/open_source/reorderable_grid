import 'package:flutter/widgets.dart';
import 'package:reorderable_grid/reorderable_grid.dart';
import 'package:reorderable_grid/src/bloc.dart';
import 'package:reorderable_grid/src/provider.dart';
import 'package:reorderable_grid/src/widgets/reorderable_target_widget.dart';

class ReorderableGrid extends StatefulWidget {
  final ReorderableGridBloc reorderableGridBloc;

  ReorderableGrid({@required this.reorderableGridBloc});

  @override
  ReorderableGridState createState() => ReorderableGridState();
}

class ReorderableGridState extends State<ReorderableGrid> {
  @override
  Widget build(BuildContext context) {
    return ReorderableGridProvider(
      reorderableGridBloc: widget.reorderableGridBloc,
      child: StreamBuilder<List<AnimatedWidgetData>>(
        initialData: widget.reorderableGridBloc.animatedData.value,
        stream: widget.reorderableGridBloc.animatedData,
        builder: (context, snapshot) {
          return GridView.count(
            crossAxisCount: 4,
            children: snapshot.data.map(
              (c) {
                return ReorderableGridTargetWidget(
                  child: c.animatedChild,
                );
              },
            ).toList(),
          );
        },
      ),
    );
  }
}
