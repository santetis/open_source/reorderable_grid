import 'package:flutter/widgets.dart';
import 'package:reorderable_grid/reorderable_grid.dart';

class GridItem extends StatelessWidget {
  final AsyncWidgetBuilder builder;

  GridItem({@required this.builder});

  @override
  Widget build(BuildContext context) {
    final reorderableGridBloc =
        ReorderableGridProvider.of(context).reorderableGridBloc;
    return StreamBuilder<Widget>(
      initialData: reorderableGridBloc.draggedWidget.value,
      stream: reorderableGridBloc.draggedWidget,
      builder: builder,
    );
  }
}
