import 'package:flutter/widgets.dart';
import 'package:reorderable_grid/src/bloc.dart';
import 'package:reorderable_grid/src/provider.dart';

typedef void OnReorder<T>(T draggedWidget, T targetWidget);

class ReorderableGridTargetWidget extends StatelessWidget {
  final Widget child;

  ReorderableGridTargetWidget({@required this.child});

  @override
  Widget build(BuildContext context) {
    final reorderableGridBloc =
        ReorderableGridProvider.of(context).reorderableGridBloc;
    return DragTarget<Widget>(
      onWillAccept: (value) {
        reorderableGridBloc.swapSink.add(Swap<Widget>(value, child));
        return true;
      },
      onAccept: (value) {
        reorderableGridBloc.swapSink.add(Swap<Widget>(value, child));
        reorderableGridBloc.draggedWidgetSink.add(null);
      },
      builder: (context, candidateData, rejectedData) => child,
    );
  }
}
