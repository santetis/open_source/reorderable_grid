import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:rxdart/rxdart.dart';

class Swap<T> {
  final T from;
  final T to;

  const Swap(this.from, this.to);
}

class AnimatedWidgetData {
  Widget child;
  Widget animatedChild;

  AnimatedWidgetData(this.child, this.animatedChild);

  int get hashCode => child.hashCode;

  bool operator ==(Object other) =>
      other is AnimatedWidgetData &&
      (child == other.child || animatedChild == other.animatedChild);
}

class ReorderableGridBloc {
  final _swapSubject = PublishSubject<Swap<Widget>>();
  final _draggedWidget = BehaviorSubject<Widget>();

  BehaviorSubject<List<Widget>> _data;
  BehaviorSubject<List<AnimatedWidgetData>> _animatedData;
  StreamSubscription<Swap> _swapDataSubscription;
  StreamSubscription<Swap> _swapAnimatedDataSubscription;

  ReorderableGridBloc({List<Widget> children: const []}) {
    _data = BehaviorSubject<List<Widget>>(seedValue: children);
    _animatedData = BehaviorSubject<List<AnimatedWidgetData>>(
        seedValue: children.map((c) => AnimatedWidgetData(c, c)).toList());
    _swapDataSubscription = _swapSubject.stream.listen((swap) {
      final _d = _data.value.map<Widget>((item) {
        if (draggedWidget.value == null) {
          return item;
        }
        if (item == swap.from) {
          return swap.to;
        } else if (item == swap.to) {
          return swap.from;
        }
        return item;
      }).toList();
      _data.add(_d);
    });

    _swapAnimatedDataSubscription = Observable.combineLatest2(
        _swapSubject.stream, draggedWidget, (swap, dWidget) {
      final _d = _animatedData.value
          .map<AnimatedWidgetData>((w) => _toAnimatedWidget(swap, w, dWidget))
          .toList();
      _animatedData.add(_d);
    }).listen(null);
  }

  ValueObservable<List<Widget>> get data => _data.stream;
  ValueObservable<List<AnimatedWidgetData>> get animatedData =>
      _animatedData.stream;
  ValueObservable<Widget> get draggedWidget => _draggedWidget.stream;

  Sink<Swap<Widget>> get swapSink => _swapSubject.sink;
  Sink<Widget> get draggedWidgetSink => _draggedWidget.sink;

  void dispose() {
    _data.close();
    _swapSubject.close();
    _draggedWidget.close();
    _animatedData.close();
    _swapDataSubscription?.cancel();
    _swapAnimatedDataSubscription?.cancel();
  }

  AnimatedWidgetData _toAnimatedWidget(
      Swap<Widget> swap, AnimatedWidgetData w, Widget dWidget) {
    if (dWidget == null) {
      return AnimatedWidgetData(w.child, w.child);
    }
    if (w.child == swap.from || w.animatedChild == swap.from) {
      return AnimatedWidgetData(
        swap.to,
        SlideWidget(
          child: swap.to,
          offset: _getOffset(swap, w),
          key: UniqueKey(),
          onCompleted: _animationCompleted,
        ),
      );
    } else if (w.child == swap.to || w.animatedChild == swap.to) {
      return AnimatedWidgetData(swap.from, swap.from);
    }
    return AnimatedWidgetData(w.child, w.child);
  }

  void _animationCompleted() {
    final _d = _animatedData.value
        .map<AnimatedWidgetData>((w) => _toAnimatedWidget(null, w, null))
        .toList();
    _animatedData.add(_d);
  }

  Offset _getOffset(Swap<Widget> swap, AnimatedWidgetData w) {
    final d = _animatedData.value;
    var toAnimated = AnimatedWidgetData(swap.to, swap.to);
    if (w.child == swap.to || w.animatedChild == swap.to) {
      toAnimated = w;
    }
    var fromAnimated = AnimatedWidgetData(swap.from, swap.from);
    if (w.child == swap.from || w.animatedChild == swap.from) {
      fromAnimated = w;
    }
    final toAnimatedIndex = d.indexOf(toAnimated);
    // final fromAnimatedIndex = d.indexOf(fromAnimated);
    if (toAnimatedIndex - 4 >= 0 && d[toAnimatedIndex - 4] == fromAnimated) {
      return Offset(0.0, 1.0);
    } else if (toAnimatedIndex + 4 < d.length &&
        d[toAnimatedIndex + 4] == fromAnimated) {
      return Offset(0.0, -1.0);
    } else if (toAnimatedIndex - 1 >= 0 &&
        d[toAnimatedIndex - 1] == fromAnimated) {
      return Offset(1.0, 0.0);
    } else if (toAnimatedIndex + 1 < d.length &&
        d[toAnimatedIndex + 1] == fromAnimated) {
      return Offset(-1.0, 0.0);
    } else if (toAnimatedIndex - 1 == -1) {
      return Offset(-1.0, 0.0);
    } else if (toAnimatedIndex + 1 == d.length) {
      return Offset(1.0, 0.0);
    }
    return Offset.zero;
  }
}

class SlideWidget extends StatefulWidget {
  final Widget child;
  final Offset offset;
  final void Function() onCompleted;

  SlideWidget({this.child, this.offset, this.onCompleted, Key key})
      : super(key: key);

  @override
  _SlideWidgetState createState() => _SlideWidgetState();
}

class _SlideWidgetState extends State<SlideWidget>
    with SingleTickerProviderStateMixin {
  Animation<Offset> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      duration: Duration(milliseconds: 200),
      vsync: this,
    );
    animation =
        Tween(begin: widget.offset, end: Offset.zero).animate(controller)
          ..addListener(() {
            setState(() {});
            if (animation.status == AnimationStatus.completed) {
              widget.onCompleted();
            }
          });
    controller.forward();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SlideTransition(
      position: animation,
      child: widget.child,
    );
  }
}
